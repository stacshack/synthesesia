import jm.JMC;
import jm.music.data.*;
import jm.util.Play;
import sun.applet.Main;

import java.util.Arrays;
import java.util.Random;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.sound.*;
import javax.sound.sampled.*;

import javafx.scene.media.*;

/* SYNTHESESIA created for StacsHack II
 * 20-21 February 2016
 * Team:
 * Ray Yang
 * Pavel Georgiev
 * Alex Annan
 * Sam Bunce
 * This file uses jMusic by
 * Andrew Sorensen and Andrew Brown
 */

public final class Synthesesia implements JMC {
	
	public static void main(String[] args) throws InterruptedException {		
		Score s = new Score(150);

		Part instrument = new Part(DROPS);
	
		//Part rhythm = new Part(PIANO);

		/*
			RANDOMLY GENERATE NEW ARRAY
			Random r = new Random();
		  	int[][] test = new int[100][1];
		  	System.out.print("Generating notes: ");
		  	for (int i=0;i<test.length;i++) {
		  		test[i][0] = r.nextInt(20);
		  		System.out.print(test[i][0]);
		  		if (i<test.length-1) System.out.print(", ");
		  	} 
		  	System.out.println();
		  	Phrase p = intToNotes(test);
		  	System.out.println(p.toString());
		 */

		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("src/img/ray.jpg"));
		} catch (IOException e) {
			System.out.println("file not found");
		}
		BufferedImage img_grayscale = ImgLoader.makeGray(img);
		
		Color[][] colormatrix = ImgLoader.getColorArray(img,8);
		//Color[][] graymatrix = ImgLoader.getColorArray(img_grayscale,8);
		
		int[][] i = colorsToInt(colormatrix);
		int[][] j = colorsToInt(graymatrix);
		System.out.println(Arrays.deepToString(i));

		/*piano.add(intToNotes(i));
		s.add(piano);
		
		//SWindow window = new SWindow(img);
		 
		try {
	        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/drop.wav").getAbsoluteFile());
	        Clip clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        clip.start();
	    } catch(Exception ex) {
	        System.out.println("Error with playing sound.");
	        ex.printStackTrace();
	    }
		
		Thread.sleep(13000);
=======
		instrument.add(intToNotes(i));
		//rhythm.add(intToNotes(j));
		//s.add(rhythm);
		s.add(instrument);
 */
		
		Play.midi(s);
	}

	public static int colorToInt(Color input) {
		return ((input.getRed() + input.getGreen() + input.getBlue()) / 3);
	}
	
	public static int[][] colorsToInt (Color[][] input) {
		int[][] output = new int[input.length][input[0].length];
		for (int i=0;i<output.length;i++) {
			for (int j=0;j<output[0].length;j++) {
				output[i][j] = colorToInt(input[i][j])/10;
			}
		}
		return output;
	}
	
	public static Phrase colorToNotes(Color[][] input) {
		int[][] output = new int[input.length][input[0].length];
		for (int i = 0; i < output.length; i++) {
			for (int j = 0; j < output[0].length; j++) {
				output[i][j] = colorToInt(input[i][j]);
			}
		}
		return intToNotes(output);
	}

	public static Phrase intToNotes(int[][] input) {
		Phrase output = new Phrase();
		int offset = 0;
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[0].length; j++) {
				if (j == 0 && i == 0) {
					offset = input[i][j];
					output.addNote(C4, QN);
				} else {
					int note = C4 + input[i][j] - offset;
					output.addNote(note, QN);
				}
			}
		}
		// add a cadence to the end
		output.addNote(G4, QN);
		output.addNote(C4, QN);
		return output;
	}

	public static void threeBlindMice() {
		Phrase p = new Phrase();
		Note[] n = new Note[33];
		n[0] = new Note(E4, QN);
		n[1] = new Note(D4, QN);
		n[2] = new Note(C4, QN);
		n[3] = new Note(E4, QN);
		n[4] = new Note(D4, QN);
		n[5] = new Note(C4, QN);
		n[6] = new Note(G4, QN);
		n[7] = new Note(F4, QN);
		n[8] = new Note(F4, QN);
		n[9] = new Note(E4, QN);
		n[10] = new Note(G4, QN);
		n[11] = new Note(C4, QN);
		n[12] = new Note(C4, QN);
		n[13] = new Note(B4, QN);
		n[14] = new Note(A4, QN);
		n[15] = new Note(B4, QN);
		n[16] = new Note(C4, QN);
		n[17] = new Note(G4, QN);
		n[18] = new Note(G4, QN);
		n[19] = new Note(G4, QN);
		n[20] = new Note(C4, QN);
		n[21] = new Note(C4, QN);
		n[22] = new Note(B4, QN);
		n[23] = new Note(A4, QN);
		n[24] = new Note(B4, QN);
		n[25] = new Note(C4, QN);
		n[26] = new Note(G4, QN);
		n[27] = new Note(G4, QN);
		n[28] = new Note(G4, QN);
		n[29] = new Note(F4, QN);
		n[30] = new Note(E4, QN);
		n[31] = new Note(D4, QN);
		n[32] = new Note(C4, QN);

		p.addNoteList(n);
		Play.midi(p);
	}
}
