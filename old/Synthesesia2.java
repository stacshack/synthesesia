import jm.JMC;
import jm.music.data.*;
import jm.util.Play;
import jm.util.View;

import java.util.Arrays;
import java.util.Random;
import java.awt.Color;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;

/* SYNTHESESIA created for StacsHack II
 * 20-21 February 2016
 * Team:
 * Ray Yang
 * Pavel Georgiev
 * Alex Annan
 * Sam Bunce
 * This file uses jMusic by
 * Andrew Sorensen and Andrew Brown
 */

public final class Synthesesia2 implements JMC {
	
	public static void main(String[] args) {
		Score s = new Score(150);
		
		Part piano = new Part(JAZZ_GUITAR);

		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("src/img/test.jpg"));
		} catch (IOException e) {
			System.out.println("file not found");
		}
		Color[][] colormatrix = ImgLoader.getColorArray(img, 5);
		
		int[][] i = colorsToInt(colormatrix);
		System.out.println(Arrays.deepToString(i));
		Phrase myPhrase = intToNotes(i);
		View.notate(myPhrase);
		piano.add(myPhrase);
		s.add(piano);
		Play.midi(s);
	}

	public static int colorToInt(Color input) {
		return ((input.getRed() + input.getGreen() + input.getBlue()) / 3);
	}
	
	public static int[][] colorsToInt (Color[][] input) {
		int[][] output = new int[input.length][input[0].length];
		for (int i=0;i<output.length;i++) {
			for (int j=0;j<output[0].length;j++) {
				output[i][j] = colorToInt(input[i][j])/10;
			}
		}
		return output;
	}

	public static Phrase colorToNotes(Color[][] input) {
		int[][] output = new int[input.length][input[0].length];
		for (int i = 0; i < output.length; i++) {
			for (int j = 0; j < output[0].length; j++) {
				output[i][j] = colorToInt(input[i][j]);
			}
		}
		return intToNotes(output);
	}
	
	public static Phrase intToNotes(int[][] input) {
		Phrase output = new Phrase();
		int offset = 0;
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[0].length; j++) {
				if (j == 0 && i == 0) {
					offset = input[i][j];
					output.addNote(C4, QN);
				} else {
					int note = C4 + input[i][j] - offset;
					if(majorKey(note))
						output.addNote(note, QN);
				}
			}
		}
		// add a cadence to the end
		output.addNote(G3, QN);
		output.addNote(C4, QN);
		return output;
	}

	public static boolean majorKey(int n) {
		int[] mode = {0,2,4,5,7,9,11,12}; //C major scale degrees
		for(int k = 0; k < mode.length; k++) {
			if(n % 12 == mode[k])
				return true;
		}
		return false;
	}
}
