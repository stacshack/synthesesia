#:poop:SYTHESESIA:poop:  
##The best way to make the worst music :sunglasses:  

###How to use
It's fairly simple. Load up the program (main in Synth3.class) and use the buttons to upload an image.  
Choose a drop from the "drop-down" menu (SEE WHAT WE DID THERE????:smirk:) and hit "Generate music". Revel at the masterpiece you have created.  
Do it all over again!  

###Created by
Raymond Yang :key:  
Pavel Georgiev :fire:  
Samuel Bunce :dromedary_camel:  

###Special Thanks
Andrew Sorensen  
Andrew Brown  
(Creators of jMusic)  
Alex Annan  
Everybody else whose name starts with A  
STACS  :pizza::pizza::pizza:  
Bitbucket :eggplant:  
And of course ourselves, who made this wonderful, life-enhancing project.

#VOTE SYNTHESESIA
#MAKE IMAGES DANK AGAIN
#:sparkles::skull::shit::ok_hand::heart: