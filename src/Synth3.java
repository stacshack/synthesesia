import jm.JMC;
import jm.music.data.*;
import jm.util.Play;
import jm.util.View;

import java.util.Arrays;
import java.util.Random;
import java.awt.Color;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Synth3 implements JMC{	
//	int[] mode = {0,2,4,5,7,9,11,12}; //C major scale degrees
	public static void main(String[] args) throws InterruptedException {
		SWindow window = new SWindow();	
		
	}
	
	public static void playImage(BufferedImage img, int tonic, int instrument) {
		Score s = new Score(128);
		Part melody = new Part(instrument);
		
		Color[][] colormatrix = ImgLoader.getColorArray(img, 8); // first get a matrix of colors from the image
		int[] raw = colorsToInts(colormatrix); // turn the colors into an array of ints
		Phrase melodyPhrase = intsToPhrase(raw, tonic); // turn the ints into a phrase
		melody.add(melodyPhrase);
		Phrase bassPhrase = new Phrase();
		s.add(melody);
		Play.midi(s);
	}
	
	public static void playImageBagpipes(BufferedImage img, int tonic) {
		Score s = new Score(150);
		int instrument = BAGPIPES; // BAG PIPES!!!!!!!!!!!!!!!1
		Part melody = new Part(instrument);
		Part bass = new Part(instrument);
		
		Color[][] colormatrix = ImgLoader.getColorArray(img, 8); // first get a matrix of colors from the image
		int[] raw = colorsToInts(colormatrix); // turn the colors into an array of ints
		Phrase melodyPhrase = intsToPhrase(raw, tonic); // turn the ints into a phrase
		melody.add(melodyPhrase);
		Phrase bassPhrase = new Phrase();
		System.out.println(melodyPhrase.getSize());
		bassPhrase.addNote(tonic % 12 + 48, melodyPhrase.getSize() + 4); // set the bass to an appropriate pitch
		bass.add(bassPhrase);
		s.add(bass);
		s.add(melody);
		Play.midi(s);
	}
	public static void playMelodyBagpipes(int tonic) {
		Score s = new Score(150);
		int instrument = BAGPIPES; // BAG PIPES!!!!!!!!!!!!!!!1
		Part melody = new Part(instrument);
		Part bass = new Part(instrument);
		
		
		int[] raw = {12,5,4,5,7,9,5,9,12,17,17,16,17,12,9,5};
		Phrase melodyPhrase = intsToPhrase(raw, tonic); // turn the ints into a phrase
		melody.add(melodyPhrase);
		Phrase bassPhrase = new Phrase();
		bassPhrase.addNote(10 % 12 + 48, 30); // set the bass to an appropriate pitch
		bass.add(bassPhrase);
		s.add(bass);
		s.add(melody);
		Play.midi(s);
	}
	// average the RGB to produce an integer between 0-15 (2 octaves)
	public static int colorToInt(Color input) {
		int raw = (input.getRed() + input.getGreen() + input.getBlue()) / 3;
		return (raw % 15);
	}
	// apply colorToInt to a 2d array and flatten
	public static int[]colorsToInts (Color[][] input) {
		int[] output = new int[input.length*input[0].length];
		int count = 0;
		for (int i=0;i<input.length;i++) {
			for (int j=0;j<input[0].length;j++) {
				output[count] = colorToInt(input[i][j]);
				count++;
			}
		}
		return output;
	}
	// convert ints from colorsToInt to a phrase
	public static Phrase intsToPhrase(int[] input, int tonic) {
		int[] mode = generateMajorMode(tonic);
		Phrase output = new Phrase();
		for(int n: input) {
			if(filterKey(n, tonic))
				output.addNote(tonic + n, QN);
		}
		return output;
	}
	// ensure all notes are in the right key
	public static boolean filterKey(int n, int tonic) {
		int[] mode = generateMajorMode(tonic);
		for(int k = 0; k < mode.length; k++) {
			if(n % 12 == mode[k])
				return true;
		}
		return false;
	}
	// generate mode from tonic key
	// int[] mode = {0,2,4,5,7,9,11,12}; //C major scale degrees
	public static int[] generateMajorMode(int tonic) {
		int[] mode = new int[8];
		mode[0] = tonic % 12;
		mode[1] = mode[0] + 2;
		mode[2] = mode[1] + 2;
		mode[3] = mode[2] + 1;
		mode[4] = mode[3] + 2;
		mode[5] = mode[4] + 2;
		mode[6] = mode[5] + 2;
		mode[7] = mode[6] + 1;
		for(int k = 0; k < mode.length; k++)
			mode[k] = mode[k] % 12;
		return mode;
	}
	
	//combines the build up and the bagpipes
	public static void playMusic(String path, String buildup) throws InterruptedException{
		int buildTime = 0;
		boolean drop = true;
		String file = "resources/";
		switch (buildup) {
		case "Star Wars" : {
			file += "starwars.wav";
			buildTime = 8000;
			break;
		}
		case "Bass Drop" : {
			file += "drop.wav"; 
			buildTime = 12750;
			break;
		}
		case "Smoke Weed Everyday" : {
			file += "smokeweed.wav";
			buildTime = 9500;
			break;
		}
		case "Airhorns" : {
			file += "airhorn.wav";
			buildTime = 1500;
			break;
		}
		default: drop = false;
		}
		if (drop) {
			try {
				AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(file).getAbsoluteFile());
				Clip clip = AudioSystem.getClip();
				clip.open(audioInputStream);
				clip.start();
			} catch(Exception ex) {
				System.out.println("Error with playing sound.");
				ex.printStackTrace();
			}
			Thread.sleep(buildTime);
		}
		playImageBagpipes(ImgLoader.loadImage(path), C5);
	}
}
