import java.awt.image.BufferedImage;
import java.io.*;
import java.awt.*;

public class ImageSplitter {

	public static BufferedImage[] splitImage(BufferedImage image,int rows, int cols) throws IOException {
		int chunks = rows * cols;

		int chunkWidth = image.getWidth() / cols; // determines the chunk width
													// and height
		int chunkHeight = image.getHeight() / rows;
		int count = 0;
		BufferedImage imgs[] = new BufferedImage[chunks]; // Image array to hold
															// image chunks
		for (int x = 0; x < rows; x++) {
			for (int y = 0; y < cols; y++) {
				// Initialize the image array with image chunks
				imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

				// draws the image chunk
				Graphics2D gr = imgs[count++].createGraphics();
				gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x,
						chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
				gr.dispose();
			}
		}
		return imgs;
	}
	public static BufferedImage[] splitImage(BufferedImage image) throws IOException {
		return splitImage(image, 4 ,4);
	}
}
