import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import java.io.File;

public class SWindow extends JFrame {
	final JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
	private JTextField log;
	private JLabel image;
	private JButton upload, play;
	private JComboBox buildup;
	private boolean flag = false;//flag to check whether image has been uploaded
	private String path;//path of the imported image

	public SWindow() {
		setTitle("Synthesesia");
		setSize(1200, 1000);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		Container pane = getContentPane();
		pane.setLayout(new GridLayout(3, 2));
		// loading content of the frame
		play = new JButton("Generate Music!");
		upload = new JButton("Upload an image");
		log = new JTextField();
		Font font1 = new Font("SansSerif", Font.BOLD, 32);
        log.setFont(font1);
		log.setEnabled(false);
		buildup = new JComboBox<String>();
		buildup.addItem("None");
		buildup.addItem("Star Wars");
		buildup.addItem("Bass Drop");
		buildup.addItem("Smoke Weed Everyday");
		buildup.addItem("Airhorns");
		// adding content of the frame
		pane.add(log);
		pane.add(upload);
		pane.add(play);
		pane.add(buildup);
		
		setVisible(true);
		addToLog(log, "Welcome to sythesesia.");

	/*	//This doesn't work! :'(
	 * this.addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
            	System.out.println("Terminating.");
        		System.exit(0);
            }
        } );
        */
		
		upload.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {

				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
					ex.printStackTrace();
				}

				JFileChooser fc = new JFileChooser();
				fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					System.out.println(fc.getSelectedFile().getPath());
				    path = relativePath(fc.getSelectedFile().getPath());
					System.out.println(path);
					JLabel image2 = new JLabel(new ImageIcon(ImgLoader.loadImage(path)));
					image = image2;
					pane.add(image2);
					String message = "Image added:"+path;
					addToLog(log,"Image added:"+path);
					revalidate();
					flag = true;
				}
			}
		});
		play.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if (flag == true) {
					try {
						Synth3.playMusic(path, (String)buildup.getSelectedItem());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		
	}

	private void addToLog(JTextField log, String message) {
		log.setText(log.getText()+"\n"+message);
	}
	
	// turns the absolute path into relative
	public static String relativePath(String path) {
		String base = System.getProperty("user.dir");
		String relative = new File(base).toURI().relativize(new File(path).toURI()).getPath();
		return relative;
	}
    //method to change log (mainly for outside of the method)
	//public void setLog(String text) {
	//	log.setText(text);
	//	add(log);
	//	revalidate();
	//}

}
