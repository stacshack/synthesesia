import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ImgLoader {
	public static void main(String[] args){
		BufferedImage myImage = loadImage("src/img/djkhaled1.png");
		Color[][] colorarray = getColorArray(myImage);
		BufferedImage myGrayscaleImage = makeGray(loadImage("src/img/djkhaled1.png"));
		Color[][] grayscaleArray = getColorArray(myGrayscaleImage);
		showArray(colorarray);
		showArray(grayscaleArray);
	}
	
	public static BufferedImage loadImage(String path){
		BufferedImage img = null;
		try {

		    img = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
	
	public static BufferedImage loadImage(File file){
		BufferedImage img = null;
		try {

		    img = ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
	
	public static BufferedImage makeGray(BufferedImage img)
	{   BufferedImage result = img;
	    for (int x = 0; x < img.getWidth(); ++x)
	    for (int y = 0; y < img.getHeight(); ++y)
	    {
	        int rgb = img.getRGB(x, y);
	        int r = (rgb >> 16) & 0xFF;
	        int g = (rgb >> 8) & 0xFF;
	        int b = (rgb & 0xFF);

	        int grayLevel = (r + g + b) / 3;
	        int gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel; 
	        result.setRGB(x, y, gray);
	    }
	    return result;
	}
	
	public static Color[][] getColorArray(BufferedImage img, int length) {
		int height = img.getHeight();
		int width = img.getWidth();
		if(width % 2 != 0) // ensure that the height and width are even
			width -= 1;
		if(height %2 != 0)
			height -=1;
		int block_width = width/length;
		int block_height = height/length;
		
		System.out.println("block width: " + block_width);
		System.out.println("block height: " + block_height);
		
		Color[][] output = new Color[height/block_height][height/block_width];
		
		for(int bx = 0; bx < width && bx/block_width < output[0].length;  bx += block_width) { // traverse big blocks
			for(int by = 0; by < height && by/block_height < output.length; by += block_height) {
			  int rSum = 0;
			  int gSum = 0;
			  int bSum = 0;
			  
			  for(int x = 0; x < block_width && x + bx < width; x++)
				  for(int y = 0; y < block_height && y + by < height; y++) {
					  Color c = new Color(img.getRGB(bx + x, by + y));
					  rSum += c.getRed();
					  gSum += c.getGreen();
					  bSum += c.getBlue();
				  }
			  int total = block_width * block_height;
			  int rAvg = rSum/total;
			  int gAvg = gSum/total;
			  int bAvg = bSum/total;
			  
//			  System.out.println("(" + bx/block_width + "," + by/block_height + ")");
			  output[by/block_height][bx/block_width] = new Color(rAvg, gAvg, bAvg);
			}
		}
		return output;
	}
	public static Color[][] getColorArray(BufferedImage img) {
		int height = img.getHeight();
		int width = img.getWidth();
		Color[][] output = new Color[height/10][width/10];
		BufferedImage[] imgs = null;
		try {
			imgs = ImageSplitter.splitImage(img, height/10, width/10);
		} catch(Exception e) {
			
		}
		int counter = 0;
		for(int i = 0; i < output.length; i++) {
			for(int j = 0; j < output[0].length; j++) {
				output[i][j] = avgColor(imgs[counter]);
				counter++;
			}
		}
		return output;
	}
	public static Color avgColor(BufferedImage img) {
		int rSum = 0;
		int gSum = 0;
		int bSum = 0;
		int total = img.getHeight() * img.getWidth();
		for(int i = 0; i < img.getHeight(); i++) {
			for(int j = 0; j < img.getWidth(); j++) {
				Color c = new Color(img.getRGB(j, i));
				rSum += c.getRed();
				gSum += c.getGreen();
				bSum += c.getBlue();
			}
		}
		int rAvg = rSum/total;
		int gAvg = gSum/total;
		int bAvg = bSum/total;
		
		return new Color(rAvg, gAvg, bAvg);
	}
	public static void showArray(Color[][] colorarray) {
		BufferedImage image = new BufferedImage(colorarray[0].length, colorarray.length, BufferedImage.TYPE_INT_ARGB);
		for(int i = 0; i < colorarray.length; i++) {
			for(int j = 0; j < colorarray[i].length; j++) {
				image.setRGB(i, j, colorarray[j][i].getRGB());
			}
		}
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().add(new JLabel(new ImageIcon(image.getScaledInstance(300, 300, Image.SCALE_SMOOTH))));
		frame.pack();
		frame.setVisible(true);
		
		
//		try {
//			ImageIO.write(image, "png", new File("output.png"));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
